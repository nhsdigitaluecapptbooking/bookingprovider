function ajaxRefresh() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("ajaxData").innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", "/poc/refresh", true);
  xhttp.send();

  document.getElementById("aud").setAttribute("value",window.location.protocol + "//" + window.location.hostname + ":443/poc");      
};

function ajaxReset() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("ajaxData").innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", "/poc/ajaxreset", true);
  xhttp.send();
};

function copyFunction() {
  /* Get the text field */
  var copyText = document.getElementById("authToken");//.getAttribute("value");

  /* Select the text field */
  copyText.select();
  copyText.setSelectionRange(0, 99999); /*For mobile devices*/

  /* Copy the text inside the text field */
  document.execCommand("copy");

  /* Alert the copied text */
  alert("Token copied to clipboard: " + copyText.value);
} 

function modalClose(objectid) {
  document.getElementById(objectid).style.visibility = "hidden";
}