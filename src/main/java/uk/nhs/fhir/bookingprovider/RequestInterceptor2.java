/*
 * Copyright 2018 NHS Digital.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.nhs.fhir.bookingprovider;

import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;

import ca.uhn.fhir.rest.api.RestOperationTypeEnum;
import ca.uhn.fhir.rest.api.server.RequestDetails;
import ca.uhn.fhir.rest.api.server.ResponseDetails;
import ca.uhn.fhir.rest.server.exceptions.AuthenticationException;
import ca.uhn.fhir.rest.server.exceptions.ForbiddenOperationException;
import ca.uhn.fhir.rest.server.interceptor.InterceptorAdapter;
import uk.nhs.fhir.bookingprovider.azure.AzureAD;
import uk.nhs.fhir.bookingprovider.logging.ExternalLogger;

/**
 * See https://hapifhir.io/doc_rest_server_interceptor.html for more details,
 * this class overrides the functions incomingRequestPreProcessed() and
 * outgoingResponse().
 * incomingRequestPreProcessed() is where the JWT is inspected and validated,
 * and also where the request has a GUID assigned to it, that follows the
 * request and response throughout processing.
 *
 * It also logs both the request and the response with this correlation GUID
 * through an ExternalLogger class which is passed into the constructor.
 *
 * The overridden outgoingResponse() method also adds the ETag to support
 * versioning.
 *
 * @author tim.coates@nhs.net
 */
public class RequestInterceptor2 extends InterceptorAdapter {

    private static final Logger LOG = Logger.getLogger(RequestInterceptor2.class.getName());
    private final String JWKURL;
    private final String ISSUER;
    AzureAD adWrangler;
    ExternalLogger ourLogger;

    /**
     * Properties file in which we store the URLs used for checking tokens.
     */
    Properties jwtURLs;

    /**
     * Constructor, just tells us to load the properties file holding all of the
     * App IDs, and the properties file which configures the AzureAD endpoints.
     *
     * The ExternalLogger we are passed in will be used to send details of both
     * the Request and the Response correlated to a GUID.
     *
     */
    public RequestInterceptor2(ExternalLogger newLogger) {
        loadJWTURLs();
        JWKURL = jwtURLs.getProperty("JWKURL");
        ISSUER = jwtURLs.getProperty("ISSUER");
        adWrangler = new AzureAD();
        ourLogger = newLogger;
    }

    /**
     * Override the incomingRequestPreProcessed method, which is called for each
     * incoming request before any processing is done.
     *
     * This is where we do the auth checking.
     *
     * @param theRequest The request we've received.
     * @param theResponse The expected response.
     * @return Returns true to allow the request to be processed, or false to
     * stop here.
     *
     * When returning False, it provides a custom error response.
     *
     */
    @Override
    public boolean incomingRequestPreProcessed(HttpServletRequest theRequest, HttpServletResponse theResponse) {

        String authHeader = theRequest.getHeader("Authorization");
        String clientName = null;
        String requestid = UUID.randomUUID().toString();

        if (authHeader != null) {
            LOG.info("JWT: " + authHeader);
            String requestURI = theRequest.getRequestURL().toString();
            String requestVerb = theRequest.getMethod();
            LOG.info("Request received " + requestURI + " " + theRequest.getQueryString());
            if (authHeader.toLowerCase().startsWith("bearer")) {
                String tokenValue = authHeader.substring(6, authHeader.length()).trim();
                clientName = validateToken(tokenValue, requestURI, requestVerb);
                if(clientName == null) {
                    throw new AuthenticationException("Authorization header not validated");
                } else {
                    theRequest.setAttribute("uk.nhs.fhir.bookingprovider.requestid", clientName  + " " + requestid);
                    ourLogger.log(clientName + " presented a valid JWT " + requestid);
                    return true;
                }
            } else {
                throw new AuthenticationException("Authorization header doesn't begin with 'Bearer'");
            }
        } else {
            throw new AuthenticationException("No 'Authorization' header received");
        }
    }

    /**
     * Method to validate the JWT we've been passed.
     *
     * @param token
     * @return
     */
      
    public final String validateToken(final String token, final String reqURI, final String reqVerb) {
        String clientName = null;
            DecodedJWT actualJWT = JWT.decode(token);       
           
            // Check it's current etc
            checkTimes(actualJWT);
           
            // Check client has the correct scope for request.
            if (!checkScope(actualJWT, reqVerb, reqURI)) {
                LOG.severe("Client does not have the correct scope to make request.");
                throw new ForbiddenOperationException("Client does not have the correct scope to make request.");
            }

            // Log the client's ID
            clientName = logAppID(actualJWT);
            //clientName = "TEST";

            // Check the token is intended for this server
            if (!checkAudience(actualJWT, reqURI)) {
                LOG.severe("Token was not intended for: " + reqURI);
                return null;
            }      

        return clientName;
    }

    /**
     * Method to check the various times; Not Before, Issued At and Expiry.
     * NB: We allow 7 minutes 30 seconds of grace on both not before and expiry
     * times to accommodate time drift.
     *
     * @param theJWT The Decoded JWT as per:
     * https://static.javadoc.io/com.auth0/java-jwt/3.3.0/com/auth0/jwt/interfaces/DecodedJWT.html
     *
     * @return Whether or not this token is current.
     *
     */
    private void checkTimes(DecodedJWT theJWT) {

        // Check it hasn't yet expired
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.SECOND, -450);
        if (theJWT.getExpiresAt().before(calendar.getTime())) {
            Calendar expiresAt = Calendar.getInstance();
            expiresAt.setTime(theJWT.getExpiresAt());
            throw new AuthenticationException("Access Token has expired");
        }

        // Check it's ready to be used
        calendar = Calendar.getInstance();
        calendar.add(Calendar.SECOND, 450); 
        if (theJWT.getNotBefore() != null) {
	        if (theJWT.getNotBefore().after(calendar.getTime())) {
	            Calendar notBefore = Calendar.getInstance();
	            notBefore.setTime(theJWT.getNotBefore());
	            throw new AuthenticationException("Access Token is not yet valid");
	        }
        }
    }
    
    /**
     * Method to check whether the claimed scope is correct for booking operations
     *
     * TODO: This should differentiate between the ability to read Slots and to
     * book an appointment.
     *
     * @param theJWT The incoming JWT
     * @return
     */
    private boolean checkScope(DecodedJWT theJWT, String rVerb, String rURI) {
        boolean canReadSlots = false;
        boolean canBookAppts = false;
        String requestType = rVerb;
        String requestURI = rURI;
        String createAppointments = "patient/appointment.write";
        String readSlots = "organization/slot.read";
    
        String scopes = theJWT.getClaim("requested_scope").asString();
        
        // Make sure the scope claim is populated
        if (scopes == null) {
        		throw new ForbiddenOperationException("The token's scope claim was null");        	
        }        
        
        // Check scopes match the requested operation
        LOG.info("Found scope: " + scopes);
        if (scopes != null) {
            if (scopes.equals(createAppointments) && (requestType.equals("POST") || requestType.equals("PUT") || (requestURI.contains("/Appointment/") && requestType.equals("GET")))) {
                canBookAppts = true;
            }
            if (scopes.equals(readSlots) && requestType.equals("GET") && !(requestURI.contains("/Appointment/"))) {
                canReadSlots = true;
            }
        }
                
        if (canBookAppts || canReadSlots) {
            LOG.info("Correct scope found, this request should be permitted.");
        } else {
            LOG.info("Scopes not found, this request should be BLOCKED.");
        }
        return (canBookAppts || canReadSlots);
    }
        
    /**
     * Method to check the token is intended for 'us'
     *
     * @param theJWT
     * @return Validates that the URL called matches that of the audience in the
     * supplied JWT.
     */
    private boolean checkAudience(DecodedJWT theJWT, String URI) {
        LOG.info("Checking JWT was intended for: " + URI);
        List<String> audienceList = theJWT.getAudience();
        boolean correctAudience = false;
        for (String audience : audienceList) {
            LOG.info("Audience: " + audience);

            // Necessary hack here, as ECS strips off the port number on the way in.
            if (audience.equals("http://appointments.directoryofservices.nhs.uk:443/poc")) {
                audience = "http://appointments.directoryofservices.nhs.uk/poc";
            }
            if (audience.equals("http://a2sibookingprovidertest.directoryofservices.nhs.uk:443/poc")) {
                audience = "http://a2sibookingprovidertest.directoryofservices.nhs.uk/poc";
            }

            if (URI.startsWith(audience)) {
                correctAudience = true;
            }
        }
        if (correctAudience) {
            LOG.info("Allowing as correct audience");
            return true;
        }
        return false;
    }

    /**
     * Method to simply log out which client made the request.
     *
     * Does a lookup into Azure AD and determine and log the name as well as the GUID.
     *
     * @param theJWT The incoming JWT
     */
    private String logAppID(DecodedJWT theJWT) {
        
    	//String clientID = theJWT.getClaim("requesting_device").asString();
    	
    	Map<String, Object> device_map = theJWT.getClaim("requesting_device").asMap();
        
    	
    	
    	String clientID = device_map.get("identifier").toString(); 
     
    	//String appName = adWrangler.getAppName(clientID);
    	String appName = "none";    	
        LOG.info("\"JWT was issued to: " + appName + " (" + clientID + ")");        
        return clientID;
    }

    /**
     * Method to load the URLs used for JWT handling, from jwt.properties file.
     *
     */
    private void loadJWTURLs() {
        InputStream input = null;
        try {
            jwtURLs = new Properties();
            ClassLoader classLoader = getClass().getClassLoader();
            input = classLoader.getResource("jwt.properties").openStream();
            jwtURLs.load(input);
        }
        catch (IOException ex) {
            LOG.severe("Error reading appid.properties file " + ex.getMessage());
        }
        finally {
            if (input != null) {
                try {
                    input.close();
                }
                catch (IOException e) {
                    LOG.severe("Error closing appid.properties file: " + e.getMessage());
                }
            }
        }
    }

    /**
     * Tells our resident Azure handler to flush any cached results it has.
     */
    public void flushAzureCache() {
        adWrangler.flushCaches();
    }

    /**
     * Intercepts all outbound (non-error) responses.
     *
     * This is where we are able to add the eTAG to support checks for versions
     * as described at: http://hl7.org/fhir/stu3/http.html#concurrency
     *
     *
     * @param theRequestDetails
     * @param theResponseDetails
     * @param theServletRequest
     * @param theServletResponse
     * @return Returns true to continue with normal processing, or false to
     *          break (e.g. if this is handling the response)
     * @throws AuthenticationException
     */
    @Override
    public boolean outgoingResponse(RequestDetails theRequestDetails,
                         ResponseDetails theResponseDetails,
                         HttpServletRequest theServletRequest,
                         HttpServletResponse theServletResponse)
                  throws AuthenticationException {

        // If the request was related to Appointment
        if(theRequestDetails.getResourceName().equals("Appointment")) {
            LOG.info("Adding ETag - Was an Appointment request");
            if(theRequestDetails.getRestOperationType() == RestOperationTypeEnum.READ) {
                LOG.info("Adding ETag - And it was a READ");
                String version = theResponseDetails.getResponseResource().getIdElement().getVersionIdPart();
                if(! version.equals("")) {
                    LOG.info("Adding ETag - Resource had a version");
                    String ETag = "W/\"" + version + "\"";
                    LOG.info("Adding ETag - Adding the ETag header: " + ETag + " to the response.");
                    theServletResponse.addHeader("ETag", ETag);
                }
            }
        }
        return true;
    }
}
