/**
 * 
 */
package uk.nhs.fhir.bookingprovider;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.Base64;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

import com.google.common.io.Files;
import com.google.gson.*;

/**
 * 
 * @author David Ruddy
 *
 */
public class bookingTokenGenerator {
	
	JsonObject header = new JsonObject();
	JsonObject payload = new JsonObject();
	String key = new String();		
	String token_header = new String();
	String token_payload = new String();
	
	String token = new String();
	
	JsonObject RequestingDevice = new JsonObject();
	JsonArray SystemIdentifier = new JsonArray();
	JsonObject SystemIdentifierSystem = new JsonObject();		
	JsonObject RequestingOrganisation = new JsonObject();
	JsonObject OrganisationIdentifier = new JsonObject();
	JsonArray OrganisationIdentifierArray = new JsonArray();
	JsonObject RequestingPractitioner = new JsonObject();
	JsonArray RequestingPractitionerIdentifier = new JsonArray();
	JsonObject RequestingPractitionerIdentifierUID = new JsonObject();
	JsonObject RequestingPractitionerIdentifierRpID = new JsonObject();
	JsonObject RequestingPractitionerIdentifierUGUID = new JsonObject();
	JsonObject Name = new JsonObject();
	JsonArray NameArray = new JsonArray();
	JsonArray NameGiven = new JsonArray();
	JsonArray NamePrefix = new JsonArray();
	long now;
	long exp; 	
	
	public String generate(String mode) {
	   	if (mode.equals("expired")) {
			now = Instant.now().getEpochSecond() - 3600;			
			exp = now + 3000;
		} else {
			now = Instant.now().getEpochSecond();
			exp = now + 3600;	
		}
			
	    header.addProperty("typ","JWT");
	    header.addProperty("alg","none");
	    
	    RequestingDevice.addProperty("resourceType", "Device");
	    SystemIdentifierSystem.addProperty("system","https://consumersupplier.com/Id/device-identifier");
	    SystemIdentifierSystem.addProperty("value","CONS-APP-4");
	    SystemIdentifier.add(SystemIdentifierSystem);
	    RequestingDevice.add("identifier", SystemIdentifier);
	    RequestingDevice.addProperty("model","Consumer product name");
	    RequestingDevice.addProperty("version","5.3.0");
	    	    
	    RequestingOrganisation.addProperty("resourceType", "Organisation");	   
	    OrganisationIdentifier.addProperty("system", "https://fhir.nhs.uk/Id/ods-organization-code");
	    OrganisationIdentifier.addProperty("value", "A1001");
	    OrganisationIdentifierArray.add(OrganisationIdentifier);
	    RequestingOrganisation.add("identifier",OrganisationIdentifierArray);
	    RequestingOrganisation.addProperty("name","Test Hospital");
	    
	    RequestingPractitioner.addProperty("resourceType","Practitioner");
	    RequestingPractitioner.addProperty("id","10019");	     
	    
	    RequestingPractitionerIdentifierUID.addProperty("system","https://fhir.nhs.uk/Id/sds-user-id");
	    RequestingPractitionerIdentifierUID.addProperty("value","111222333444");
	    RequestingPractitionerIdentifierRpID.addProperty("system","https://fhir.nhs.uk/Id/sds-role-profile-id");
	    RequestingPractitionerIdentifierRpID.addProperty("value", "444555666777");
	    RequestingPractitionerIdentifierUGUID.addProperty("system","https://consumersupplier.com/Id/user-guid");
	    RequestingPractitionerIdentifierUGUID.addProperty("value","98ed4f78-814d-4266-8d5b-cde742f3093c");
	    RequestingPractitionerIdentifier.add(RequestingPractitionerIdentifierUID);
	    RequestingPractitionerIdentifier.add(RequestingPractitionerIdentifierRpID);
	    RequestingPractitionerIdentifier.add(RequestingPractitionerIdentifierUGUID);
	    Name.addProperty("family","Jones");
	    NameGiven.add("Claire");
	    NamePrefix.add("Dr");
	    Name.add("given",NameGiven);
	    Name.add("prefix",NamePrefix);
	    NameArray.add(Name);
	    
	    RequestingPractitioner.add("identifier",RequestingPractitionerIdentifier);
	    RequestingPractitioner.add("Name",NameArray);
	      
	    
	    payload.addProperty("iss","https://consumersupplier.thirdparty.nhs.uk/");
	    payload.addProperty("sub","10019");
	    payload.addProperty("aud","http://appointments.directoryofservices.nhs.uk:443/poc");
	    payload.addProperty("exp",exp);
	    payload.addProperty("iat",now);
	    payload.addProperty("reason_for_request","directcare");
	    payload.addProperty("requested_scope","organization/slot.read");
	    payload.add("requesting_device",RequestingDevice);	    
	    payload.add("requesting_organization",RequestingOrganisation);
	    payload.add("requesting_practitioner",RequestingPractitioner);
	    
	    key = "";	      
	    
		token_header = Base64.getUrlEncoder().withoutPadding().encodeToString(header.toString().getBytes(StandardCharsets.UTF_8));
		token_payload = Base64.getUrlEncoder().withoutPadding().encodeToString(payload.toString().getBytes(StandardCharsets.UTF_8));
		
		token = token_header + "." + token_payload + "." + key;
		
		return token;
	}
	
	public String generate(
			 String mode
			,String iss
			,String aud
			,String reqReason
			,String reqScope
			,String systemIdentifierSystem
			,String systemIdentifierValue
			,String requestingDeviceModel
			,String requestingDeviceVesion
			,String requestingOrganisationODSCode
			,String requestingOrganisationName
			,String practitionerID
			,String practitionerSDSuserID
			,String practitionerSDSroleID
			,String practitionerUserSystemIDtype
			,String practitionerUserSystemIDvalue
			,String practitionerNameFamily
			,String practitionerNameGiven
			,String practitionerNamePrefix
	) {		
		
		if (mode.equals("expired")) {
			now = Instant.now().getEpochSecond() - 3600;			
			exp = now + 3600;
		} else {
			now = Instant.now().getEpochSecond();
			exp = now + 3600;	
		}
				
		header.addProperty("typ","JWT");
	    header.addProperty("alg","none");
	    
	    RequestingDevice.addProperty("resourceType", "Device");
	    SystemIdentifierSystem.addProperty("system",systemIdentifierSystem);
	    SystemIdentifierSystem.addProperty("value",systemIdentifierValue);
	    SystemIdentifier.add(SystemIdentifierSystem);
	    RequestingDevice.add("identifier", SystemIdentifier);
	    RequestingDevice.addProperty("model",requestingDeviceModel);
	    RequestingDevice.addProperty("version",requestingDeviceVesion);
	    	    
	    RequestingOrganisation.addProperty("resourceType", "Organisation");	   
	    OrganisationIdentifier.addProperty("system", "https://fhir.nhs.uk/Id/ods-organization-code");
	    OrganisationIdentifier.addProperty("value", requestingOrganisationODSCode);
	    OrganisationIdentifierArray.add(OrganisationIdentifier);
	    RequestingOrganisation.add("identifier",OrganisationIdentifierArray);
	    RequestingOrganisation.addProperty("name",requestingOrganisationName);
	    
	    RequestingPractitioner.addProperty("resourceType","Practitioner");
	    RequestingPractitioner.addProperty("id",practitionerID);	     
	    
	    RequestingPractitionerIdentifierUID.addProperty("system","https://fhir.nhs.uk/Id/sds-user-id");
	    RequestingPractitionerIdentifierUID.addProperty("value",practitionerSDSuserID);
	    RequestingPractitionerIdentifierRpID.addProperty("system","https://fhir.nhs.uk/Id/sds-role-profile-id");
	    RequestingPractitionerIdentifierRpID.addProperty("value", practitionerSDSroleID);
	    RequestingPractitionerIdentifierUGUID.addProperty("system",practitionerUserSystemIDtype);
	    RequestingPractitionerIdentifierUGUID.addProperty("value",practitionerUserSystemIDvalue);
	    RequestingPractitionerIdentifier.add(RequestingPractitionerIdentifierUID);
	    RequestingPractitionerIdentifier.add(RequestingPractitionerIdentifierRpID);
	    RequestingPractitionerIdentifier.add(RequestingPractitionerIdentifierUGUID);
	    Name.addProperty("family",practitionerNameFamily);
	    NameGiven.add(practitionerNameGiven);
	    NamePrefix.add(practitionerNamePrefix);
	    Name.add("given",NameGiven);
	    Name.add("prefix",NamePrefix);
	    NameArray.add(Name);
	    
	    RequestingPractitioner.add("identifier",RequestingPractitionerIdentifier);
	    RequestingPractitioner.add("Name",NameArray);
	      	    
	    payload.addProperty("iss",iss);	    
	    payload.addProperty("sub",RequestingPractitioner.get("id").getAsString());
	    payload.addProperty("aud",aud);
	    payload.addProperty("exp",exp);
	    payload.addProperty("iat",now);	    
	    payload.addProperty("reason_for_request",reqReason);
	    payload.addProperty("requested_scope",reqScope);
	    payload.add("requesting_device",RequestingDevice);	    
	    payload.add("requesting_organization",RequestingOrganisation);
	    payload.add("requesting_practitioner",RequestingPractitioner);
	    
	    key = "";	      
	    
		token_header = Base64.getUrlEncoder().withoutPadding().encodeToString(header.toString().getBytes(StandardCharsets.UTF_8));
		token_payload = Base64.getUrlEncoder().withoutPadding().encodeToString(payload.toString().getBytes(StandardCharsets.UTF_8));
		
		token = token_header + "." + token_payload + "." + key;
		
		return token;		
	}
}