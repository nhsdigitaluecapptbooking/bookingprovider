package uk.nhs.fhir.bookingprovider;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.IOException;


public class DebugOutput {

	private String path;

	public DebugOutput () {
		path = "debug_file.txt";
	}
	
	public void DebugWriter (String debug_label, String debug_value) throws IOException {
		FileWriter write = new FileWriter (path, false);
		PrintWriter debug_line = new PrintWriter (write);
		debug_line.printf("%s" + "%n", debug_label + ": " + debug_value);
		debug_line.close();		
	}
}